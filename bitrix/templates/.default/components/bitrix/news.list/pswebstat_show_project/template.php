<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<?

$bullet_arr = array("round", "square", "triangleUp", "triangleDown", "triangleLeft", "triangleRight", "bubble", "diamond", "xError", "yError");
foreach ($arResult["QUERIES"] as $query_id => $arItem) {
    $bullet_counter = 0; // счётчик для распределения иконок поисковым системам

    ?>
    <div class="chart_cont">
        <h3><?= $arItem["NAME"] ?></h3>
        <script type="text/javascript">
            $(function () {
                var chart = AmCharts.makeChart("chart_<?=$query_id?>", {
                        "type": "serial",
                        "theme": "light",
                        "legend": {
                            "useGraphSettings": true
                        },
                        "pathToImages": "/bitrix/js/main/amcharts/3.3/images/",
                        "dataDateFormat": "YYYY-MM-DD",
                        "valueAxes": [
                            {
                                "axisAlpha": 0,
                                "position": "left",
                                "reversed": true,
                                "dashLength": 5,
                                "gridCount": 10,
                                "position": "left",
                                "title": "Позиция"
                            }
                        ],
                        "graphs": [
                                <?
                                $query = array();
                                $first_eng_id = false;
                                foreach($arItem["SENGINES"] as $key => $sengine){
                                    $sengine_name = $sengine["SENGINE_NAME"].($sengine["SENGINE_REGION"]?"(".$sengine["SENGINE_REGION"].")":"");
                                    if(!$first_eng_id){
                                        $first_eng_id = $sengine["SENGINE_CODE"];
                                    }
                                    foreach($sengine["POSITIONS"] as $date => $pos){
                                        $query[$date][$sengine["SENGINE_CODE"]] = $pos["POSITION"];
                                    }
                                    ?>{
                                "id": "<?=$sengine["SENGINE_CODE"]?>",
                                "bullet": "<?=$bullet_arr[$bullet_counter++%10]?>",
                                "bulletBorderAlpha": 1,
                                "bulletSize": 5,
                                "hideBulletsCount": 50,
                                "lineThickness": 2,
                                "title": "<?=$sengine_name?>",
                                "useLineColorForBulletBorder": true,
                                "valueField": "se_<?=$sengine["SENGINE_CODE"]?>",
                                "balloonText": "Позиция в <?=$sengine_name?> - [[value]]",
                            },
                            <?}
                                               ?>
                        ],
                        "chartScrollbar": {
                            "graph": "<?=$first_eng_id?>",
                            "scrollbarHeight": 30
                        },
                        "chartCursor": {
                            "cursorPosition": "mouse",
                            "pan": true
                        },
                        "categoryField": "date",
                        "categoryAxis": {
                            "parseDates": true,
                            "dashLength": 1,
                            "minorGridEnabled": true,
                            "position": "top"
                        },
                        exportConfig: {
                            menuRight: '20px',
                            menuBottom: '50px',
                            menuItems: [
                                {
                                    icon: '/bitrix/js/main/amcharts/3.3/images/export.png',
                                    format: 'png'
                                }
                            ]
                        },
                        "dataProvider": [
                                <?
                            ksort($query);
                            foreach($query as $date => $sengine){
                                ?>{
                                "date": "<?=$date?>", <?
                            foreach($sengine as $engine => $position){
                                ?>
                                "se_<?=$engine?>": <?=$position?>,
                                <?
                            }
                            ?>},
                            <?
                                                    }
                                                ?>
                        ]
                    }
                );

                chart.addListener("rendered", zoomChart);

                zoomChart();
                function zoomChart() {
                    chart.zoomToIndexes(chart.dataProvider.length - 40, chart.dataProvider.length - 1);
                }
            })
        </script>

        <? /* хайчартс
        <script type="text/javascript">
            $(function () {
                $('#chart_<?=$query_id?>').highcharts({
                    title: {
                        text: '<?=$arItem["NAME"]?>',
                        x: -20 //center
                    },
                    subtitle: {
                        text: 'ID фразы: <?=$arItem["ID"]?>',
                        x: -20
                    },
                    xAxis: {
                        categories: [<?
                        $dates = array();
                        foreach($arItem["SENGINES"] as $sengine){
                            foreach($sengine["POSITIONS"] as $date => $position){
                                $dates[] = $date;
                            }
                        }
                        $dates = array_unique($dates);
                        asort($dates);
                        foreach($dates as $date){
                            $dt = explode("-",$date);
                            echo "'".$dt[2].".".$dt[1]."', ";
                        }
                    ?>]
                    },
                    yAxis: {
                        title: {
                            text: 'Позиция'
                        },
                        plotLines: [{
                            value: 1,
                            width: 1,
                            color: '#808080'
                        }],
                        reversed: true
                    },
                    tooltip: {
                        valueSuffix: '#'
                    },
                    legend: {
                        layout: 'vertical',
                        align: 'right',
                        verticalAlign: 'middle',
                        borderWidth: 0
                    },
                    series: [<?
                    foreach($arItem["SENGINES"] as $sengine){
                        ?>{
                        name: '<?=$sengine["SENGINE_NAME"].($sengine["SENGINE_REGION"]?"(".$sengine["SENGINE_REGION"].")":"")?>',
                        data: [<?
                                foreach($sengine['POSITIONS'] as $position){
                                    echo $position["POSITION"].", ";
                                }
                            ?>]
                    },<?
                    }
                ?>]
                });
            });
        </script>*/
        ?>
        <div class="chart_container" id="chart_<?= $query_id ?>"></div>
    </div>
<? } ?>