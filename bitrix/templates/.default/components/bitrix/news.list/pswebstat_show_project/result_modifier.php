<?
$QUERIES = array();
if(count($arResult["ITEMS"])){
    foreach($arResult["ITEMS"] as $arItem){
        /*echo "<pre>";
        print_r($arItem);
        echo "</pre><hr>";
        */

        /*
         * //поля элемента инфоблока.
         * $FIELDS = array(
            "ID"                    => $arItem["ID"],
            "IBLOCK_ID"             => $arItem["IBLOCK_ID"],
            "IBLOCK_SECTION_ID"     => $arItem["IBLOCK_SECTION_ID"],
            "NAME"                  => $arItem["NAME"],
            "ACTIVE_FROM"           => $arItem["ACTIVE_FROM"],
            "DETAIL_PAGE_URL"       => $arItem["DETAIL_PAGE_URL"],
            "DETAIL_TEXT"           => $arItem["DETAIL_TEXT"],
            "DETAIL_TEXT_TYPE"      => $arItem["DETAIL_TEXT_TYPE"],
            "PREVIEW_TEXT"          => $arItem["PREVIEW_TEXT"],
            "PREVIEW_TEXT_TYPE"     => $arItem["PREVIEW_TEXT_TYPE"],
            "SORT"                  => $arItem["SORT"],
            "CODE"                  => $arItem["CODE"],
            "DISPLAY_ACTIVE_FROM"   => $arItem["DISPLAY_ACTIVE_FROM"],
        );*/

        foreach($arItem["PROPERTIES"] as $prop_code => $property){
            if(strpos($prop_code, "SENGINE_") === false){ // обычные свойства
                /* не нужны для формирования массива запросов
                 * $FIELDS[$prop_code] = array(
                    "NAME" => $prop_code["NAME"],
                    "VALUE" => $prop_code["VALUE"]
                );*/
            }
            else{ // свойства с позициями
                $VALUES = array();
                foreach($property["VALUE"] as $val){
                    $POS_ARR = explode("|||",$val);
                    if(!isset($QUERIES[$POS_ARR[4]])){
                        $QUERIES[$POS_ARR[4]] = array(
                            "NAME"  => $POS_ARR[3],
                            "ID"    => $POS_ARR[4],
                            "SENGINES" => array()
                        );
                    }

                    if(!isset($QUERIES[$POS_ARR[4]]["SENGINES"][$POS_ARR[1]])){
                        $QUERIES[$POS_ARR[4]]["SENGINES"][$POS_ARR[1]] = array(
                                "SENGINE_NAME" 		=> $POS_ARR[0],
                                "SENGINE_CODE" 		=> $POS_ARR[1],
                                "SENGINE_REGION"    => $POS_ARR[2],
                                "POSITIONS"         => array()
                        );
                    }

                    if(!isset($QUERIES[$POS_ARR[4]]["SENGINES"][$POS_ARR[1]]["POSITIONS"][$POS_ARR[10]])){
                        $QUERIES[$POS_ARR[4]]["SENGINES"][$POS_ARR[1]]["POSITIONS"][$POS_ARR[10]] = array(
                            "QUERY_WORDSTAT" 	=> $POS_ARR[5],
                            "POSITION"			=> $POS_ARR[6],
                            "PREV_POSITION"		=> $POS_ARR[7],
                            "CHANGE_POSITION"	=> $POS_ARR[8],
                            "URL"		        => $POS_ARR[9],
                        );
                    }

                }
                $FIELDS[$prop_code] = array(
                    "NAME" => $prop_code["NAME"],
                    "VALUE" => $prop_code["VALUE"]
                );
            }
        }
    }
}
$arResult["QUERIES"] = $QUERIES;
?>