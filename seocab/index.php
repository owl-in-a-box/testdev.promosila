<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("SEO кабинет");

?>

<?
if(1 || $GLOBALS["USER"]->IsAuthorized()){ /*TODO запретить просмотр неавторизованным пользователям*/

    $APPLICATION->IncludeComponent("bitrix:main.user.link","",Array(
            "CACHE_TYPE" => "N",
            "CACHE_TIME" => "7200",
            "ID" => "",
            "NAME_TEMPLATE" => "#NOBR##LAST_NAME# #NAME##/NOBR#",
            "SHOW_LOGIN" => "Y",
            "THUMBNAIL_LIST_SIZE" => "30",
            "THUMBNAIL_DETAIL_SIZE" => "100",
            "USE_THUMBNAIL_LIST" => "Y",
            "SHOW_FIELDS" => Array("PERSONAL_BIRTHDAY", "PERSONAL_ICQ", "PERSONAL_PHOTO", "PERSONAL_CITY", "WORK_COMPANY", "WORK_POSITION", "UF_PSWEBSTAT_PROJECT"),
            "USER_PROPERTY" => Array("UF_USER_CAR_DEMO"),
            "PATH_TO_SONET_USER_PROFILE" => "",
            "PROFILE_URL" => "",
            "DATE_TIME_FORMAT" => "d.m.Y H:i:s",
            "SHOW_YEAR" => "Y"
        )
    );

    $GLOBALS['arFilter'] = array(
        "PROPERTY_USER_ID" => $_REQUEST["USER_ID"]?$_REQUEST["USER_ID"]:$GLOBALS["USER"]->GetId(),
        ">DATE_ACTIVE_FROM" => "10.06.2014",
    );

    // получение id инфоблока
    // TODO хранить в настройках модуля
    CModule::IncludeModule("pswebstat");
    $api_obj = new \Owlinabox\pswebstat\PSWebStatChecker;
    $ib_id = $api_obj->IB_ID;

    // выбираемые свойства
    $SELECT_PROPS = array(
        "SEREQ_COUNT",
        "SEREQ_TOP3",
        "SEREQ_TOP10",
        "SEREQ_TOP30",
        "SEREQ_DOWN",
        "SEREQ_UP",
    );

    // получение свойств с позициями
    $SENGINES = $api_obj->GetIbProps($ib_id, "SENGINE");
    foreach($SENGINES as $search_engine){
        $SELECT_PROPS[] = $search_engine["CODE"];
    }

    $APPLICATION->IncludeComponent("bitrix:news.list", "pswebstat_show_project", Array(
        "DISPLAY_DATE" => "Y",	// Выводить дату элемента
        "DISPLAY_NAME" => "Y",	// Выводить название элемента
        "DISPLAY_PICTURE" => "Y",	// Выводить изображение для анонса
        "DISPLAY_PREVIEW_TEXT" => "Y",	// Выводить текст анонса
        "AJAX_MODE" => "Y",	// Включить режим AJAX
        "IBLOCK_TYPE" => "news",	// Тип информационного блока (используется только для проверки)
        "IBLOCK_ID" => $ib_id,	// Код информационного блока
        "NEWS_COUNT" => "",	// Количество новостей на странице
        "SORT_BY1" => "ACTIVE_FROM",	// Поле для первой сортировки новостей
        "SORT_ORDER1" => "DESC",	// Направление для первой сортировки новостей
        "SORT_BY2" => "SORT",	// Поле для второй сортировки новостей
        "SORT_ORDER2" => "ASC",	// Направление для второй сортировки новостей
        "FILTER_NAME" => "arFilter",	// Фильтр
        "FIELD_CODE" => array(	// Поля
            0 => "ID",
        ),
        "PROPERTY_CODE" => $SELECT_PROPS,
        "CHECK_DATES" => "Y",	// Показывать только активные на данный момент элементы
        "DETAIL_URL" => "",	// URL страницы детального просмотра (по умолчанию - из настроек инфоблока)
        "PREVIEW_TRUNCATE_LEN" => "",	// Максимальная длина анонса для вывода (только для типа текст)
        "ACTIVE_DATE_FORMAT" => "d.m.Y",	// Формат показа даты
        "SET_TITLE" => "Y",	// Устанавливать заголовок страницы
        "SET_STATUS_404" => "Y",	// Устанавливать статус 404, если не найдены элемент или раздел
        "INCLUDE_IBLOCK_INTO_CHAIN" => "Y",	// Включать инфоблок в цепочку навигации
        "ADD_SECTIONS_CHAIN" => "Y",	// Включать раздел в цепочку навигации
        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",	// Скрывать ссылку, если нет детального описания
        "PARENT_SECTION" => "",	// ID раздела
        "PARENT_SECTION_CODE" => "",	// Код раздела
        "INCLUDE_SUBSECTIONS" => "Y",	// Показывать элементы подразделов раздела
        "CACHE_TYPE" => "A",	// Тип кеширования
        "CACHE_TIME" => "3600",	// Время кеширования (сек.)
        "CACHE_FILTER" => "Y",	// Кешировать при установленном фильтре
        "CACHE_GROUPS" => "Y",	// Учитывать права доступа
        "DISPLAY_TOP_PAGER" => "Y",	// Выводить над списком
        "DISPLAY_BOTTOM_PAGER" => "Y",	// Выводить под списком
        "PAGER_TITLE" => "Новости",	// Название категорий
        "PAGER_SHOW_ALWAYS" => "Y",	// Выводить всегда
        "PAGER_TEMPLATE" => "",	// Шаблон постраничной навигации
        "PAGER_DESC_NUMBERING" => "Y",	// Использовать обратную навигацию
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",	// Время кеширования страниц для обратной навигации
        "PAGER_SHOW_ALL" => "Y",	// Показывать ссылку "Все"
        "AJAX_OPTION_JUMP" => "N",	// Включить прокрутку к началу компонента
        "AJAX_OPTION_STYLE" => "Y",	// Включить подгрузку стилей
        "AJAX_OPTION_HISTORY" => "N",	// Включить эмуляцию навигации браузера
        "AJAX_OPTION_ADDITIONAL" => "",	// Дополнительный идентификатор
	),
	false
);

}
?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>