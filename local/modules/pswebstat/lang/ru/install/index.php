<?
$MESS ['PSWEBSTAT_INSTALL_IB_TYPE_NAME'] = "PSWebStat";
$MESS ['PSWEBSTAT_INSTALL_IB_TYPE_SECTION'] = "Проект";
$MESS ['PSWEBSTAT_INSTALL_IB_TYPE_ELEMENT'] = "Отчёт";
$MESS ['PSWEBSTAT_MODULE_NAME'] = "PS WebStat";
$MESS ['PSWEBSTAT_MODULE_DESCRIPTION'] = "Модуль работы с веб-статистикой";
$MESS ['PSWEBSTAT_INSTALL_TITLE'] = "Установка модуля PS WebStat";
$MESS ['PSWEBSTAT_UNINSTALL_TITLE'] = "Деинсталляция модуля PS WebStat";
$MESS ['MOD_UNINST_SAVE_IB'] = "Сохранить данные";

$MESS ['PSWEBSTAT_INSTALL_IB_CREATED'] = "Для хранения данных был создан инфоблок.";
$MESS ['PSWEBSTAT_INSTALL_IB_EXISTS'] = "Был найден существующий инфоблок модуля.";
$MESS ['PSWEBSTAT_INSTALL_IB_WRONG_VERSION'] = "Был найден и отключён инфоблок другой версии модуля. Модуль будет работать с новым инфоблоком";

$MESS ['PSWEBSTAT_IB_DESCR'] = "Инфоблок модуля PS WebStat";
?>