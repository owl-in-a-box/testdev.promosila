<?php

IncludeModuleLangFile(__FILE__);
if (class_exists("pswebstat"))
	return;


class pswebstat extends CModule
{
    var $MODULE_ID = "pswebstat";
    var $MODULE_IB_ID = "pswebstat";
    var $MODULE_IB_CODE = "pswebstat";
    var $MODULE_IB_XML_ID;
	var $MODULE_VERSION;
	var $MODULE_VERSION_DATE;
	var $MODULE_NAME;
	var $MODULE_DESCRIPTION;
	var $MODULE_CSS;
	var $MODULE_GROUP_RIGHTS = "N";
    var $SITE_ID = "s1";

    /*TODO вынести настройки в настройки модуля*/

	function pswebstat()
	{
		$arModuleVersion = array();
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));
		include($path."/version.php");
		$this->MODULE_VERSION = $arModuleVersion["VERSION"];
		$this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
		$this->MODULE_NAME = GetMessage("PSWEBSTAT_MODULE_NAME");
		$this->MODULE_DESCRIPTION = GetMessage("PSWEBSTAT_MODULE_DESCRIPTION");
        $this->MODULE_IB_XML_ID = $this->MODULE_IB_CODE."_".$this->MODULE_VERSION;
	}
	
	function GetModuleTasks()
	{
		return array();
	}


    //Создание инфоблока
    function InstallInfoBlock($arParams = array())
    {
        CModule::IncludeModule("iblock");

        //создание типа инфоблока
        $db_iblock_type = CIBlockType::GetList(
            Array("SORT" => "ASC"),
            Array("=ID" => $this->MODULE_IB_ID)
        );
        if($ar_iblock_type = $db_iblock_type->Fetch()) // тип уже существует
        {
            // дополнительные проверки
        }
        else{ //создаём тип инфоблока
            $arFields = Array(
                'ID' => $this->MODULE_IB_ID,
                'SECTIONS'=>'Y',
                'IN_RSS'=>'N',
                'SORT'=>100,
                'LANG'=>Array(
                    'ru'=>Array(
                        'NAME'=> GetMessage("PSWEBSTAT_INSTALL_IB_TYPE_NAME"),
                        'SECTION_NAME'=> GetMessage("PSWEBSTAT_INSTALL_IB_TYPE_SECTION"),
                        'ELEMENT_NAME'=> GetMessage("PSWEBSTAT_INSTALL_IB_TYPE_ELEMENT")
                    )
                )
            );

            $obBlocktype = new CIBlockType;
            $res = $obBlocktype->Add($arFields);
            if(!$res)
            {
                echo 'Error: '.$obBlocktype->LAST_ERROR.'<br>';
                return false;
            }
        }

        //создание инфоблока

        $return_message = GetMessage('PSWEBSTAT_INSTALL_IB_CREATED');
        $mark_valid_ib = false;
        $res = CIBlock::GetList( //проверяем наличие инфоблока
            Array(),
            Array(
                'TYPE' => $this->MODULE_IB_ID,
                'SITE_ID' => $this->SITE_ID,
                'CODE'=> $this->MODULE_IB_CODE
            ), true
        );
        while($ar_res = $res->Fetch())
        {
            if($ar_res['XML_ID'] == $this->MODULE_IB_XML_ID){ //проверяем версию модуля
                $mark_valid_ib = true;
                if($ar_res["ACTIVE"] != "Y"){ //акитивируем инфоблок, если он отключен
                    CIBlock::Update(
                        $ar_res['ID'],
                        array("ACTIVE" => "Y")
                     );
                    $return_message = GetMessage('PSWEBSTAT_INSTALL_IB_EXISTS');
                }
            }
            else{ //деактивируем инфоблок старой версии
                CIBlock::Update(
                    $ar_res['ID'],
                    array("ACTIVE" => "N")
                );
                $return_message = GetMessage('PSWEBSTAT_INSTALL_IB_WRONG_VERSION');
            }
        }
        if(!$mark_valid_ib){// если инфоблок не найден

            $ib = new CIBlock;

            // Настройка доступа
            $arAccess = array(
                "2" => "R", // Все пользователи
            );

            $arFields = Array(
                "ACTIVE" => "Y",

                "NAME" => GetMessage('PSWEBSTAT_INSTALL_IB_TYPE_NAME'),
                "CODE" => $this->MODULE_IB_CODE,
                "XML_ID" => $this->MODULE_IB_XML_ID,
                "IBLOCK_TYPE_ID" => $this->MODULE_IB_ID,
                "DESCRIPTION" => GetMessage('PSWEBSTAT_IB_DESCR'),
                "DESCRIPTION_TYPE" => "TEXT",

                "SITE_ID" => $this->SITE_ID,
                "SORT" => "5",
                "GROUP_ID" => $arAccess, // Права доступа
                "FIELDS" => array(
                    "DETAIL_PICTURE" => array(
                        "IS_REQUIRED" => "N", // не обязательное
                        "DEFAULT_VALUE" => array(
                            "SCALE" => "Y", // возможные значения: Y|N. Если равно "Y", то изображение будет отмасштабировано.
                            "WIDTH" => "600", // целое число. Размер картинки будет изменен таким образом, что ее ширина не будет превышать значения этого поля.
                            "HEIGHT" => "600", // целое число. Размер картинки будет изменен таким образом, что ее высота не будет превышать значения этого поля.
                            "IGNORE_ERRORS" => "Y", // возможные значения: Y|N. Если во время изменения размера картинки были ошибки, то при значении "N" будет сгенерирована ошибка.
                            "METHOD" => "resample", // возможные значения: resample или пусто. Значение поля равное "resample" приведет к использованию функции масштабирования imagecopyresampled, а не imagecopyresized. Это более качественный метод, но требует больше серверных ресурсов.
                            "COMPRESSION" => "95", // целое от 0 до 100. Если значение больше 0, то для изображений jpeg оно будет использовано как параметр компрессии. 100 соответствует наилучшему качеству при большем размере файла.
                        ),
                    ),
                    "PREVIEW_PICTURE" => array(
                        "IS_REQUIRED" => "N", // не обязательное
                        "DEFAULT_VALUE" => array(
                            "SCALE" => "Y", // возможные значения: Y|N. Если равно "Y", то изображение будет отмасштабировано.
                            "WIDTH" => "140", // целое число. Размер картинки будет изменен таким образом, что ее ширина не будет превышать значения этого поля.
                            "HEIGHT" => "140", // целое число. Размер картинки будет изменен таким образом, что ее высота не будет превышать значения этого поля.
                            "IGNORE_ERRORS" => "Y", // возможные значения: Y|N. Если во время изменения размера картинки были ошибки, то при значении "N" будет сгенерирована ошибка.
                            "METHOD" => "resample", // возможные значения: resample или пусто. Значение поля равное "resample" приведет к использованию функции масштабирования imagecopyresampled, а не imagecopyresized. Это более качественный метод, но требует больше серверных ресурсов.
                            "COMPRESSION" => "95", // целое от 0 до 100. Если значение больше 0, то для изображений jpeg оно будет использовано как параметр компрессии. 100 соответствует наилучшему качеству при большем размере файла.
                            "FROM_DETAIL" => "Y", // возможные значения: Y|N. Указывает на необходимость генерации картинки предварительного просмотра из детальной.
                            "DELETE_WITH_DETAIL" => "Y", // возможные значения: Y|N. Указывает на необходимость удаления картинки предварительного просмотра при удалении детальной.
                            "UPDATE_WITH_DETAIL" => "Y", // возможные значения: Y|N. Указывает на необходимость обновления картинки предварительного просмотра при изменении детальной.
                        ),
                    ),
                    "SECTION_PICTURE" => array(
                        "IS_REQUIRED" => "N", // не обязательное
                        "DEFAULT_VALUE" => array(
                            "SCALE" => "Y", // возможные значения: Y|N. Если равно "Y", то изображение будет отмасштабировано.
                            "WIDTH" => "235", // целое число. Размер картинки будет изменен таким образом, что ее ширина не будет превышать значения этого поля.
                            "HEIGHT" => "235", // целое число. Размер картинки будет изменен таким образом, что ее высота не будет превышать значения этого поля.
                            "IGNORE_ERRORS" => "Y", // возможные значения: Y|N. Если во время изменения размера картинки были ошибки, то при значении "N" будет сгенерирована ошибка.
                            "METHOD" => "resample", // возможные значения: resample или пусто. Значение поля равное "resample" приведет к использованию функции масштабирования imagecopyresampled, а не imagecopyresized. Это более качественный метод, но требует больше серверных ресурсов.
                            "COMPRESSION" => "95", // целое от 0 до 100. Если значение больше 0, то для изображений jpeg оно будет использовано как параметр компрессии. 100 соответствует наилучшему качеству при большем размере файла.
                            "FROM_DETAIL" => "Y", // возможные значения: Y|N. Указывает на необходимость генерации картинки предварительного просмотра из детальной.
                            "DELETE_WITH_DETAIL" => "Y", // возможные значения: Y|N. Указывает на необходимость удаления картинки предварительного просмотра при удалении детальной.
                            "UPDATE_WITH_DETAIL" => "Y", // возможные значения: Y|N. Указывает на необходимость обновления картинки предварительного просмотра при изменении детальной.
                        ),
                    ),
                    // Символьный код элементов
                    "CODE" => array(
                        "IS_REQUIRED" => "N", // Обязательное
                        "DEFAULT_VALUE" => array(
                            "UNIQUE" => "Y", // Проверять на уникальность
                            "TRANSLITERATION" => "Y", // Транслитерировать
                            "TRANS_LEN" => "30", // Максмальная длина транслитерации
                            "TRANS_CASE" => "L", // Приводить к нижнему регистру
                            "TRANS_SPACE" => "-", // Символы для замены
                            "TRANS_OTHER" => "-",
                            "TRANS_EAT" => "Y",
                            "USE_GOOGLE" => "N",
                        ),
                    ),
                    // Символьный код разделов
                    "SECTION_CODE" => array(
                        "IS_REQUIRED" => "N",
                        "DEFAULT_VALUE" => array(
                            "UNIQUE" => "Y",
                            "TRANSLITERATION" => "Y",
                            "TRANS_LEN" => "30",
                            "TRANS_CASE" => "L",
                            "TRANS_SPACE" => "-",
                            "TRANS_OTHER" => "-",
                            "TRANS_EAT" => "Y",
                            "USE_GOOGLE" => "N",
                        ),
                    ),
                    "DETAIL_TEXT_TYPE" => array(      // Тип детального описания
                        "DEFAULT_VALUE" => "html",
                    ),
                    "SECTION_DESCRIPTION_TYPE" => array(
                        "DEFAULT_VALUE" => "html",
                    ),
                    "IBLOCK_SECTION" => array(         // Привязка к разделам обязательноа
                        "IS_REQUIRED" => "N",
                    ),
                    "LOG_SECTION_ADD" => array("IS_REQUIRED" => "Y"), // Журналирование
                    "LOG_SECTION_EDIT" => array("IS_REQUIRED" => "Y"),
                    "LOG_SECTION_DELETE" => array("IS_REQUIRED" => "Y"),
                    "LOG_ELEMENT_ADD" => array("IS_REQUIRED" => "Y"),
                    "LOG_ELEMENT_EDIT" => array("IS_REQUIRED" => "Y"),
                    "LOG_ELEMENT_DELETE" => array("IS_REQUIRED" => "Y"),
                ),

                // Шаблоны страниц
                "LIST_PAGE_URL" => "#SITE_DIR#/pswebstat/",
                "SECTION_PAGE_URL" => "#SITE_DIR#/pswebstat/#SECTION_CODE#/",
                "DETAIL_PAGE_URL" => "#SITE_DIR#/pswebstat/#SECTION_CODE#/#ELEMENT_CODE#/",

                "INDEX_SECTION" => "Y", // Индексировать разделы для модуля поиска
                "INDEX_ELEMENT" => "Y", // Индексировать элементы для модуля поиска

                "VERSION" => 1, // Хранение элементов в общей таблице

                "ELEMENT_NAME" => "Отчет",
                "ELEMENTS_NAME" => "Отчеты",
                "ELEMENT_ADD" => "Добавить отчет",
                "ELEMENT_EDIT" => "Изменить отчет",
                "ELEMENT_DELETE" => "Удалить отчет",
                "SECTION_NAME" => "Категории",
                "SECTIONS_NAME" => "Категория",
                "SECTION_ADD" => "Добавить категорию",
                "SECTION_EDIT" => "Изменить категорию",
                "SECTION_DELETE" => "Удалить категорию",

                "SECTION_PROPERTY" => "Y", // Разделы каталога имеют свои свойства
            );

            $ID = $ib->Add($arFields);

            if(!($ID > 0)){
                return false;
            }
            else{
                //=======================//
                //     Cвойства отчёта   //
                //-----------------------//
                //[count] - количество   //
                //[top3]      запросов   //
                //[top10]                //
                //[top30]                //
                //[down]  - опустилось   //
                //[up]    - поднялось    //
                //=======================//

                $ibp = new CIBlockProperty;
                $arFields = Array(
                    "NAME" => "Количество поисковых запросов",
                    "ACTIVE" => "Y",
                    "MULTIPLE" => "N",
                    "SORT" => 50,
                    "CODE" => "SEREQ_COUNT",
                    "PROPERTY_TYPE" => "S",
                    "IBLOCK_ID" => $ID,
                );
                $ibp->Add($arFields);

                $arFields = Array(
                    "NAME" => "Количество запросов в Топ-3",
                    "ACTIVE" => "Y",
                    "MULTIPLE" => "N",
                    "SORT" => 51,
                    "CODE" => "SEREQ_TOP3",
                    "PROPERTY_TYPE" => "S",
                    "IBLOCK_ID" => $ID,
                );
                $ibp->Add($arFields);

                $arFields = Array(
                    "NAME" => "Количество запросов в Топ-10",
                    "ACTIVE" => "Y",
                    "MULTIPLE" => "N",
                    "SORT" => 52,
                    "CODE" => "SEREQ_TOP10",
                    "PROPERTY_TYPE" => "S",
                    "IBLOCK_ID" => $ID,
                );
                $ibp->Add($arFields);

                $arFields = Array(
                    "NAME" => "Количество запросов в Топ-30",
                    "ACTIVE" => "Y",
                    "MULTIPLE" => "N",
                    "SORT" => 53,
                    "CODE" => "SEREQ_TOP30",
                    "PROPERTY_TYPE" => "S",
                    "IBLOCK_ID" => $ID,
                );
                $ibp->Add($arFields);

                $arFields = Array(
                    "NAME" => "Количество опустившихся запросов",
                    "ACTIVE" => "Y",
                    "MULTIPLE" => "N",
                    "SORT" => 54,
                    "CODE" => "SEREQ_DOWN",
                    "PROPERTY_TYPE" => "S",
                    "IBLOCK_ID" => $ID,
                );
                $ibp->Add($arFields);

                $arFields = Array(
                    "NAME" => "Количество поднявшихся запросов",
                    "ACTIVE" => "Y",
                    "MULTIPLE" => "N",
                    "SORT" => 55,
                    "CODE" => "SEREQ_UP",
                    "PROPERTY_TYPE" => "S",
                    "IBLOCK_ID" => $ID,
                );
                $ibp->Add($arFields);

                $arFields = Array(
                    "NAME" => "ID пользователя",
                    "ACTIVE" => "Y",
                    "MULTIPLE" => "N",
                    "SORT" => 55,
                    "CODE" => "USER_ID",
                    "PROPERTY_TYPE" => "S",
                    "IBLOCK_ID" => $ID,
                );
                $ibp->Add($arFields);


                /*
                //====================//
                // Добавляем свойства //
                //    ( примеры )     //
                //====================//

                // Определяем, есть ли у инфоблока свойства
                $dbProperties = CIBlockProperty::GetList(array(), array("IBLOCK_ID"=>$ID));
                if ($dbProperties->SelectedRowsCount() <= 0)
                {
                    $ibp = new CIBlockProperty;

                    $arFields = Array(
                        "NAME" => "[служебное] Спецпредложение",
                        "ACTIVE" => "Y",
                        "SORT" => -777, // Сортировка
                        "CODE" => "SPECIALOFFER",
                        "PROPERTY_TYPE" => "L", // Список
                        "LIST_TYPE" => "C", // Тип списка - "флажки"
                        "FILTRABLE" => "Y", // Выводить на странице списка элементов поле для фильтрации по этому свойству
                        "VALUES" => array(
                            "VALUE" => "да",
                        ),
                        "IBLOCK_ID" => $ID
                    );
                    $propId = $ibp->Add($arFields);
                    if ($propId > 0)
                    {
                        $arFields["ID"] = $propId;
                        $arCommonProps[$arFields["CODE"]] = $arFields;
                        echo "&mdash; Добавлено свойство ".$arFields["NAME"]."<br />";
                    }
                    else
                        echo "&mdash; Ошибка добавления свойства ".$arFields["NAME"]."<br />";


                    $arFields = Array(
                        "NAME" => "[служебное] Дополнительные фотографии",
                        "ACTIVE" => "Y",
                        "MULTIPLE" => "Y",
                        "SORT" => -777,
                        "CODE" => "MORE_PHOTO",
                        "PROPERTY_TYPE" => "F", // Файл
                        "FILE_TYPE" => "jpg, gif, bmp, png, jpeg",
                        "IBLOCK_ID" => $ID,
                        "HINT" => "Допускается произвольное число дополнительных фотографий. Добавьте одну, и появится поле для добавленя следующей.",
                    );
                    $propId = $ibp->Add($arFields);
                    if ($propId > 0)
                    {
                        $arFields["ID"] = $propId;
                        $arCommonProps[$arFields["CODE"]] = $arFields;
                        echo "&mdash; Добавлено свойство ".$arFields["NAME"]."<br />";
                    }
                    else
                        echo "&mdash; Ошибка добавления свойства ".$arFields["NAME"]."<br />";


                    $arFields = Array(
                        "NAME" => "[служебное] Рекомендуемые товары",
                        "ACTIVE" => "Y",
                        "MULTIPLE" => "Y",
                        "SORT" => -777,
                        "CODE" => "RECOMMEND",
                        "MULTIPLE_CNT" => 2, // Количество свойств, предлагаемых по умолчанию
                        "PROPERTY_TYPE" => "E", // Привязка к элементам инфоблока
                        "IBLOCK_ID" => $ID,
                        "LINK_IBLOCK_ID" => $ID,
                        "HINT" => "Данные товары будут показываться дла этого товара как рекомендуемые на странице просмотра товара",
                    );
                    $propId = $ibp->Add($arFields);
                    if ($propId > 0)
                    {
                        $arFields["ID"] = $propId;
                        $arCommonProps[$arFields["CODE"]] = $arFields;
                        echo "&mdash; Добавлено свойство ".$arFields["NAME"]."<br />";
                    }
                    else
                        echo "&mdash; Ошибка добавления свойства ".$arFields["NAME"]."<br />";


                    // сео-свойства
                    $arFields = Array(
                        "NAME" => "[служебное] Сео-заголовок (title)",
                        "ACTIVE" => "Y",
                        "SORT" => -777,
                        "CODE" => "SEO_TITLE",
                        "PROPERTY_TYPE" => "S", // Строка
                        "ROW_COUNT" => 1, // Количество строк
                        "COL_COUNT" => 60, // Количество столбцов
                        "IBLOCK_ID" => $ID,
                        "HINT" => "Если задан - то заголовок для товара будет подставляться из этой строчки",
                    );
                    $propId = $ibp->Add($arFields);
                    if ($propId > 0)
                    {
                        $arFields["ID"] = $propId;
                        $arCommonProps[$arFields["CODE"]] = $arFields;
                        echo "&mdash; Добавлено свойство ".$arFields["NAME"]."<br />";
                    }
                    else
                        echo "&mdash; Ошибка добавления свойства ".$arFields["NAME"]."<br />";

                    $arFields = Array(
                        "NAME" => "[служебное] Ключевые слова (keywords)",
                        "ACTIVE" => "Y",
                        "SORT" => -777,
                        "CODE" => "SEO_KEYWORDS",
                        "PROPERTY_TYPE" => "S", // Строка
                        "ROW_COUNT" => 3, // Количество строк
                        "COL_COUNT" => 70, // Количество столбцов
                        "IBLOCK_ID" => $ID,
                        "HINT" => "Ключевые слова (keywords) для страницы товара",
                    );
                    $propId = $ibp->Add($arFields);
                    if ($propId > 0)
                    {
                        $arFields["ID"] = $propId;
                        $arCommonProps[$arFields["CODE"]] = $arFields;
                        echo "&mdash; Добавлено свойство ".$arFields["NAME"]."<br />";
                    }
                    else
                        echo "&mdash; Ошибка добавления свойства ".$arFields["NAME"]."<br />";

                    $arFields = Array(
                        "NAME" => "[служебное] Описание (description)",
                        "ACTIVE" => "Y",
                        "SORT" => -777,
                        "CODE" => "SEO_DESCRIPTION",
                        "PROPERTY_TYPE" => "S", // Строка
                        "ROW_COUNT" => 3, // Количество строк
                        "COL_COUNT" => 70, // Количество столбцов
                        "IBLOCK_ID" => $ID,
                        "HINT" => "Сео-описание (description) для страницы товара",
                    );
                    $propId = $ibp->Add($arFields);
                    if ($propId > 0)
                    {
                        $arFields["ID"] = $propId;
                        $arCommonProps[$arFields["CODE"]] = $arFields;
                        echo "&mdash; Добавлено свойство ".$arFields["NAME"]."<br />";
                    }
                    else
                        echo "&mdash; Ошибка добавления свойства ".$arFields["NAME"]."<br />";
                }
                else
                    echo "&mdash; Для данного инфоблока уже существуют свойства<br />";
                */

                /*
                //===================//
                // Свойства разделов //
                //===================//

                $obUserType = new CUserTypeEntity();

                $fieldTitle = "Сео-заголовок (title)";
                $arPropFields = array(
                    "ENTITY_ID" => "IBLOCK_".$ID."_SECTION",
                    "FIELD_NAME" => "UF_SEO_TITLE",
                    "USER_TYPE_ID" => "string",
                    "XML_ID" => "",
                    "SORT" => -777,
                    "MULTIPLE" => "N", // Множественное
                    "MANDATORY" => "N", // Обязательное
                    "SHOW_FILTER" => "S",
                    "SHOW_IN_LIST" => "Y",
                    "EDIT_IN_LIST" => "Y",
                    "IS_SEARCHABLE" => "N",
                    "SETTINGS" => array(
                        "SIZE" => "70", // длина поля ввода
                        "ROWS" => "1" // высота поля ввода
                    ),
                    "EDIT_FORM_LABEL" => array("ru" => $fieldTitle, "en" => ""),
                    "LIST_COLUMN_LABEL" => array("ru" => $fieldTitle, "en" => ""),
                    "LIST_FILTER_LABEL" => array("ru" => $fieldTitle, "en" => ""),
                );
                $FIELD_ID = $obUserType->Add($arPropFields);

                $fieldTitle = "Ключевые слова (keywords)";
                $arPropFields = array(
                    "ENTITY_ID" => "IBLOCK_".$ID."_SECTION",
                    "FIELD_NAME" => "UF_SEO_KEYWORDS",
                    "USER_TYPE_ID" => "string",
                    "XML_ID" => "",
                    "SORT" => -777,
                    "MULTIPLE" => "N", // Множественное
                    "MANDATORY" => "N", // Обязательное
                    "SHOW_FILTER" => "S",
                    "SHOW_IN_LIST" => "Y",
                    "EDIT_IN_LIST" => "Y",
                    "IS_SEARCHABLE" => "N",
                    "SETTINGS" => array(
                        "SIZE" => "70", // длина поля ввода
                        "ROWS" => "3" // высота поля ввода
                    ),
                    "EDIT_FORM_LABEL" => array("ru" => $fieldTitle, "en" => ""),
                    "LIST_COLUMN_LABEL" => array("ru" => $fieldTitle, "en" => ""),
                    "LIST_FILTER_LABEL" => array("ru" => $fieldTitle, "en" => ""),
                );
                $FIELD_ID = $obUserType->Add($arPropFields);

                $fieldTitle = "Описание (description)";
                $arPropFields = array(
                    "ENTITY_ID" => "IBLOCK_".$ID."_SECTION",
                    "FIELD_NAME" => "UF_SEO_DESCRIPTION",
                    "USER_TYPE_ID" => "string",
                    "XML_ID" => "",
                    "SORT" => -777,
                    "MULTIPLE" => "N", // Множественное
                    "MANDATORY" => "N", // Обязательное
                    "SHOW_FILTER" => "S",
                    "SHOW_IN_LIST" => "Y",
                    "EDIT_IN_LIST" => "Y",
                    "IS_SEARCHABLE" => "N",
                    "SETTINGS" => array(
                        "SIZE" => "70", // длина поля ввода
                        "ROWS" => "3" // высота поля ввода
                    ),
                    "EDIT_FORM_LABEL" => array("ru" => $fieldTitle, "en" => ""),
                    "LIST_COLUMN_LABEL" => array("ru" => $fieldTitle, "en" => ""),
                    "LIST_FILTER_LABEL" => array("ru" => $fieldTitle, "en" => ""),
                );
                $FIELD_ID = $obUserType->Add($arPropFields);
                */
            }
        }
        return $return_message;
    }
    function UnInstallInfoBlock($arParams = array())
    {
        global $DB;
        CModule::IncludeModule("iblock");
        if($arParams["save_ib"] != "Y"){
            //удаляем инфоблок
            $res = CIBlock::GetList( //проверяем наличие инфоблока
                Array(),
                Array(
                    'TYPE' => $this->MODULE_IB_ID,
                    'SITE_ID' => $this->SITE_ID,
                    'CODE' => $this->MODULE_IB_CODE,
                ), true
            );
            if($ar_res = $res->Fetch())
            {
                $DB->StartTransaction();
                if(!CIBlock::Delete($ar_res["ID"]))
                {
                    $DB->Rollback();
                }
                else
                    $DB->Commit();
            }

            //удаляем тип инфоблока
            CIBlockType::Delete($this->MODULE_IB_ID);
        }
        return true;
    }

    //Добавление дополнительных полей
    function InstallUserFields($arParams = array())
    {
        $obUserField  = new CUserTypeEntity;
        $res = $obUserField->GetList(array(),array("FIELD_NAME" => "UF_PSWEBSTAT_PROJECT"));
        $ar_res = $res->fetch();
        if(!$ar_res["ID"]){ // поле не найдено
            $arFields = Array(
               "ENTITY_ID" => "USER",
               "USER_TYPE_ID" => "string",
               "SORT" => "100",
               "SHOW_FILTER" => "N",
                "MULTIPLE" => "Y",
               "SETTINGS" => array(),
               "FIELD_NAME" => "UF_PSWEBSTAT_PROJECT"
            );
            $ID = $obUserField->Add($arFields);
        }

        return true;
    }
    function UnInstallUserFields($arParams = array())
    {
        $obUserField  = new CUserTypeEntity;
        $res = $obUserField->GetList(array(),array("FIELD_NAME" => "UF_PSWEBSTAT_PROJECT"));
        $ar_res = $res->fetch();
        if($ar_res["ID"]){
            $obUserField->Delete($ar_res["ID"]);
        }
        return true;
    }

    //Установка агентов
    function InstallAgents($arParams = array())
    {
        CAgent::AddAgent(
            "\Owlinabox\pswebstat\PSWebStatChecker::WebStatAgent();",    // имя функции
            "pswebstat",                            // идентификатор модуля
            "Y",                                    // агент не критичен к кол-ву запусков
            86400,                                  // интервал запуска - 1 сутки
            "",                                     // дата первой проверки на запуск //date('d.m.Y H:i:s');
            "Y",                                    // агент активен
            "",                                     // дата первого запуска
            30);                                    // сортировка
        return true;
    }
    function UnInstallAgents($arParams = array())
    {
        CAgent::RemoveModuleAgents("pswebstat");
        return true;
    }

    //добавление событий
	function InstallEvents()
	{
		return true;
	}
	
	function UnInstallEvents()
	{
		return true;
	}

    //копирование файлов
	function InstallFiles($arParams = array())
	{
		CopyDirFiles($_SERVER["DOCUMENT_ROOT"]."/local/modules/pswebstat/install/components/", $_SERVER["DOCUMENT_ROOT"]."/local/components", true, true);
		return true;
	}
	
	function UnInstallFiles()
	{
		DeleteDirFilesEx($_SERVER["DOCUMENT_ROOT"]."/local/modules/pswebstat/install/components/", $_SERVER["DOCUMENT_ROOT"]."/local/components");
		return true;
	}

    //основная функция установки
	function DoInstall()
	{
		global $USER, $APPLICATION;

		if ($USER->IsAdmin())
		{
            if($GLOBALS["messages"] = $this->InstallInfoBlock()){
                RegisterModule("pswebstat");
                CModule::IncludeModule("pswebstat");
                $this->InstallAgents();
                $this->InstallEvents();
                $this->InstallFiles();
                $this->InstallUserFields();
            }
			$GLOBALS["errors"] = $this->errors;
			$APPLICATION->IncludeAdminFile(GetMessage("PSWEBSTAT_INSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/local/modules/pswebstat/install/step.php");
		}
	}
	
	function DoUninstall()
	{
		global $DB, $USER, $DOCUMENT_ROOT, $APPLICATION, $step;
		if ($USER->IsAdmin())
		{
			$step = IntVal($step);
			if ($step < 2)
			{
				$APPLICATION->IncludeAdminFile(GetMessage("PSWEBSTAT_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/local/modules/pswebstat/install/unstep1.php");
			}
			elseif ($step == 2)
			{
                if($_REQUEST["save_ib"] != "Y"){
                    $this->UnInstallUserFields();
                    $this->UnInstallInfoBlock(array(
                        "save_ib" => $_REQUEST["save_ib"],
                    ));
                }
				$this->UnInstallEvents();
                $this->UnInstallAgents();
				$this->UnInstallFiles();
                UnRegisterModule("pswebstat");
				$GLOBALS["errors"] = $this->errors;
				$APPLICATION->IncludeAdminFile(GetMessage("PSWEBSTAT_UNINSTALL_TITLE"), $_SERVER["DOCUMENT_ROOT"]."/local/modules/pswebstat/install/unstep2.php");
			}
		}
	}
}
