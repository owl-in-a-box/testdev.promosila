<form action="<?echo $APPLICATION->GetCurPage();?>">
	<?echo bitrix_sessid_post(); ?>
	<input type="hidden" name="lang" value="<?echo LANGUAGE_ID ?>">
	<input type="hidden" name="id" value="pswebstat">
	<input type="hidden" name="uninstall" value="Y">
	<input type="hidden" name="step" value="2">
	<?echo CAdminMessage::ShowMessage(GetMessage("MOD_UNINST_WARN")); ?>
	<p><?echo GetMessage("MOD_UNINST_SAVE"); ?></p>
	<p><input type="checkbox" name="save_ib" id="save_ib" value="Y" checked><label for="save_ib"><?echo GetMessage("MOD_UNINST_SAVE_IB"); ?></label></p>
	<input type="submit" name="inst" value="<?echo GetMessage("MOD_UNINST_DEL"); ?>">
</form>