<?php
/**
 * 
 */

namespace Owlinabox\PSWebStat;

use Bitrix\Main\Application;
use Bitrix\Main\DB\MssqlConnection;
use Bitrix\Main\Entity;

class PSWebStatChecker extends Entity\DataManager
{
    var $API_key = "fb39ee84e0120174ecacc04975886819"; /*TODO Брать из настроек модуля*/
    var $API_lib;
    var $IB_ID;

    var $MODULE_IB_ID = "pswebstat";
    var $MODULE_IB_CODE = "pswebstat";
    var $SITE_ID = "s1";

    function __construct() {
        $this->API_lib = $_SERVER["DOCUMENT_ROOT"]."/local/modules/pswebstat/lib/xmlrpc.inc";
        $this->IB_ID = $this->GetIbId($this->MODULE_IB_ID, $this->MODULE_IB_CODE, $this->SITE_ID);
    }

    public static function GetFilePath()
    {
        return __FILE__;
    }

    public static function GetIbId($MODULE_IB_ID, $MODULE_IB_CODE, $SITE_ID){
        \CModule::IncludeModule("iblock");
        $res = \CIBlock::GetList( //проверяем наличие инфоблока
            Array(),
            Array(
                'TYPE' => $MODULE_IB_ID,
                'SITE_ID' => $SITE_ID,
                'CODE'=> $MODULE_IB_CODE
            ), true
        );
        if($ar_res = $res->Fetch())
        {
            return $ar_res['ID'];
        }
        else{
            return false;
        }
    }

    public static function GetIbProps($IBLOCK_ID, $XML_ID){
        $props_array = array();
        if($IBLOCK_ID){
            \CModule::IncludeModule("iblock");
            $properties = \CIBlock::GetProperties(
                $IBLOCK_ID,
                Array(),
                Array("ACTIVE"=>"Y", "XML_ID" => $XML_ID)
            );
            while ($prop_fields = $properties->GetNext())
            {
                $props_array[str_replace($XML_ID."_", "", $prop_fields["CODE"])] = array(
                    "NAME"      => $prop_fields["NAME"],             // название
                    "CODE"      => $prop_fields["CODE"],             // код поля
                    "HINT"      => $prop_fields["HINT"],             // доп. поле 1 (регион, статистика слов)
                    "LFIELD"    => $prop_fields["DEFAULT_VALUE"],  // доп. поле 2
                    "PROP_ID"   => $prop_fields["ID"],
                );
            }
        }
        return $props_array;
    }

    // функция агента модуля
    public static function WebStatAgent($pageNum = 1)
    {
        // выборка $nPageSize проектов, которые пора обновить
        // $nPageSize задаётся в настройках модуля
        // Ограничение требуется, чтобы
        // распределить нагрузку и снизить
        // время генерации страницы, не заставляя
        // пользователя ждать обновления всех
        // проектов на его хите
        \CModule::IncludeModule("main");

        $nPageSize = 1; /*TODO взять из настроек модуля*/

        $filter = Array(
            "ACTIVE" => "Y",
            "!UF_PSWEBSTAT_PROJECT" => false,    //пользовательское свойство
        );

        $nav = array(
            'iNumPage' => $pageNum, // итерация
            'nPageSize' => $nPageSize,
        );

        $rsUsers = \CUser::GetList(
            ($by="id"),
            ($order="desc"),
            $filter,
            array(
                "NAV_PARAMS" => $nav,
                "SELECT" => array("UF_PSWEBSTAT_PROJECT"),
                "FIELDS" => array("ID")
            )
        );

        if($rsUsers->is_filtered){ // если фильтр сработал
            $rsUsers->NavStart();
            while($ar_res = $rsUsers->NavNext()){
                if(is_array($ar_res['UF_PSWEBSTAT_PROJECT'])){
                    foreach($ar_res['UF_PSWEBSTAT_PROJECT'] as $project){
                        if($project){
                            PSWebStatChecker::CheckProject(array(
                                "USER_ID" => $ar_res['ID'],
                                "PROJECT_URL" => $project,
                                "ITERATION" => $pageNum
                            ));
                        }
                    }
                }
                else{
                    if($ar_res['UF_PSWEBSTAT_PROJECT']){
                        PSWebStatChecker::CheckProject(array(
                            "USER_ID" => $ar_res['ID'],
                            "PROJECT_URL" => $ar_res['UF_PSWEBSTAT_PROJECT'],
                            "ITERATION" => $pageNum
                        ));
                    }
                }
            }
        }

        if($rsUsers->NavPageCount > $pageNum){
            // регистрация второго агента, если
            // записей для проверки >= $nPageSize
            // таким образом основной агент
            // будет загружать по $nPageSize Записей на хит
            // и рекурсивно создавать одноразовых
            // агентов для оставшихся проектов
            \CAgent::AddAgent(
                "\Owlinabox\pswebstat\PSWebStatChecker::WebStatAgent(".($pageNum + 1).");",    // имя функции
                "pswebstat",                            // идентификатор модуля
                "Y",                                    // агент не критичен к кол-ву запусков
                10,                                     // интервал запуска
                "",                                     // дата первой проверки на запуск
                "Y",                                    // агент активен
                "",                                     // дата первого запуска
                30);
        }
        if($pageNum > 1){
            \CAgent::RemoveAgent("\Owlinabox\pswebstat\PSWebStatChecker::WebStatAgent(".$pageNum.")", "pswebstat");
            return "";
        }
        else{
            return "\Owlinabox\pswebstat\PSWebStatChecker::WebStatAgent();";
        }
    }

    // функция проверки одного проекта
	public static function CheckProject($PROJECT)
    {

        // дата отчета
        $req_date = $PROJECT["REQ_DATE"]?$PROJECT["REQ_DATE"]:date("Y-m-d");

        /*TODO перенести ID Инфоблока в настройки модуля, чтобы не выбирать модуль гетлистом*/
        $MODULE_IB_ID = "pswebstat";
        $MODULE_IB_CODE = "pswebstat";
        $SITE_ID = "s1";

        \CModule::IncludeModule("iblock");
        $res = \CIBlock::GetList( //проверяем наличие инфоблока
            Array(),
            Array(
                'TYPE' => $MODULE_IB_ID,
                'SITE_ID' => $SITE_ID,
                'CODE'=> $MODULE_IB_CODE
            ), true
        );
        if($ar_res = $res->Fetch())
        {
            if(!$this){
                $api_obj = new \Owlinabox\pswebstat\PSWebStatChecker;
            }
            else
                $api_obj = $this;
            $arProjects = $api_obj->API_cmd(array("CMD" => "get_projects"));

            $project_id = 0;

            foreach($arProjects as $project){
                if($project['url'] == $PROJECT["PROJECT_URL"]){
                    $project_id = $project['id_project'];
                    break;
                }
            }

            if($project_id){

                $res = $api_obj->API_cmd(array(
                    "CMD" => "get_report",
                    "PARAMS" => array (new \xmlrpcval ($project_id, "int" ), new \xmlrpcval ($req_date), "string" ))
                );

                // Дата отчета
                global $DB;
                $DATE = $DB->FormatDate($req_date, "YYYY-MM-DD", \CSite::GetDateFormat("SHORT"));

                //название отчета
                $REPORT_NAME = "user(".$PROJECT["USER_ID"].") ".$PROJECT["PROJECT_URL"]." - ".$PROJECT['ITERATION']." iteration ( ".$DATE." )";
                $REPORT_CODE = $PROJECT["USER_ID"]."/".$project_id."/".$req_date;

                // Проверка уже загруженного отчета
                $check_arSelect = Array();
                $check_arFilter = Array(
                    "IBLOCK_ID"         => $api_obj->IB_ID,
                    "ACTIVE_DATE"       => "Y",
                    "ACTIVE"            => "Y",
                    "%DATE_ACTIVE_FROM" => $DATE,
                    "CODE"              => $REPORT_CODE,
                );
                $check_res = \CIBlockElement::GetList(Array(), $check_arFilter, false, Array("nPageSize"=>50), $check_arSelect);
                if(!$check_res->GetNextElement()) // Если такой отчет ещё не загружен
                {
                    $PROPS = array(
                        "SEREQ_COUNT"   => $res['count'],
                        "SEREQ_TOP3"    => $res['top3'],
                        "SEREQ_TOP10"   => $res['top10'],
                        "SEREQ_TOP30"   => $res['top30'],
                        "SEREQ_DOWN"    => $res['down'],
                        "SEREQ_UP"      => $res['up'],
                        "USER_ID"       => $PROJECT["USER_ID"]
                    );

                    // Поисковые системы
                    $SENGINES = $api_obj->GetIbProps($api_obj->IB_ID, "SENGINE");

                    $is_all_fields = true; // маркер наличия всех полей
                    foreach($res["sengines"] as $enginge){
                        if(!isset($SENGINES[$enginge['id_se']])){
                            /*TODO Унифицировать поля с различными регионами поиска*/
                            $is_all_fields = false;
                            $arFields = Array(
                                "NAME" => $enginge['name_se'],
                                "ACTIVE" => "Y",
                                "SORT" => "100",
                                "CODE" => "SENGINE_".$enginge['id_se'],
                                "PROPERTY_TYPE" => "S",
                                "MULTIPLE" => "Y",
                                "IBLOCK_ID" => $api_obj->IB_ID,
                                "XML_ID" => "SENGINE",
                                "HINT" => $enginge['name_region'],
                                "DEFAULT_VALUE" => "",
                            );

                            $iblockproperty = new \CIBlockProperty;
                            $PropertyID = $iblockproperty->Add($arFields);
                        }
                    }
                    if(!$is_all_fields)
                        $SENGINES = $api_obj->GetIbProps($api_obj->IB_ID, "SENGINE");

                    // Запросы
                    $QUERIES = array();
                    foreach ($res['queries'] as $query){
                        $QUERIES[$query['id_query']] = array(
                            "NAME" => $query['query'],
                            "WORDSTAT" => $query['wordstat']
                        );
                    }

                    // Позиции
                    foreach($res['positions'] as $seid_qid => $position){
                        $ids = explode("_", $seid_qid);
                        $sengine_id = $ids[0];
                        $query_id = $ids[1];
                        $PROPS[$SENGINES[$sengine_id]["PROP_ID"]][] = implode("|||", array(
                            "SENGINE_NAME" 		=> $SENGINES[$sengine_id]["NAME"],
                            "SENGINE_CODE" 		=> $sengine_id,
                            "SENGINE_REGION" 	=> $SENGINES[$sengine_id]["HINT"],
                            "QUERY_NAME" 		=> $QUERIES[$query_id]["NAME"],
                            "QUERY_ID"          => $query_id,
                            "QUERY_WORDSTAT" 	=> $QUERIES[$query_id]["WORDSTAT"],
                            "POSITION"			=> $position['position'],
                            "PREV_POSITION"		=> $position['prev_position'],
                            "CHANGE_POSITION"	=> $position['change_position'],
                            "URL"				=> $position['url'],
                            "DATE"				=> $req_date
                        ));
                    }

                    // добавление элемента в инфоблок

                    $ibEl = new \CIBlockElement;
                    $ibEl->Add(
                        array(
                            "ACTIVE" => "Y",
                            "IBLOCK_ID" => $ar_res["ID"],
                            "DATE_ACTIVE_FROM" => $DATE,
                            "NAME" => $REPORT_NAME,
                            "CODE" => $REPORT_CODE,
                            //"PREVIEW_TEXT" => print_r($res, true),
                            "PROPERTY_VALUES" => $PROPS,
                        ),
                        false,
                        true,
                        false
                    );

                }
            }
        }
    }

    public function API_Connect(){

        $API_key = $this->API_key;
        require_once($this->API_lib);

        $client = new \xmlrpc_client ( "api", "allpositions.ru", 80 );
        $GLOBALS ['xmlrpc_defencoding'] = "UTF8";
        $GLOBALS ['xmlrpc_internalencoding'] = "UTF-8";

        $client->setcookie ( 'api_key', $API_key, '/', 'allpositions.ru' );

        return $client;
    }

    public function API_cmd($arParams){
        $msg = $arParams["MSG"];
        $params = isset($arParams["PARAMS"])?$arParams["PARAMS"]:"";
        $client = $this->API_Connect();

        $msg = new \xmlrpcmsg ( $msg, $params );
        $resp = $client->send ( $msg );
        if ($resp->faultCode ())
            exit ( $resp->faultString () );
        $result = php_xmlrpc_decode ( $resp->value () );
        return($result);
    }

	public static function ValidateSiteName()
	{
        IncludeModuleLangFile(__FILE__);
		return array(
			new Entity\Validator\RegExp(
                '/^(([a-z0-9\-\.]+)?[a-z0-9\-]+(!?\.[a-z]{2,4}))$/',
                GetMessage('PSWEBSTAT_SITENAME_INVALID')
            )
		);
	}
}